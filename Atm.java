public class Atm {
        int allMoney;
        int maxMoney;
        int giveMoney;
        int transaction;

        Atm(int maxMoney, int allMoney, int giveMoney) {
            this.maxMoney = maxMoney;
            this.allMoney = allMoney;
            this.giveMoney = giveMoney;
        }
        void giveMe(int value) {
            if (value < 1)
                System.out.println("Деньги не выданы");
            else if (value > giveMoney) {
                System.out.println("Привышен лимит суммы выдачи. Максимальная сумма 1500!");
            }
            else if (value <= giveMoney && value <= allMoney) {
                this.transaction ++;
                this.allMoney -= value;
                System.out.println("Выдана сумма: " + value);
            } else System.out.println("Недостаточно денег в банкомате");
        }
        void putMoney(int value) {
            if (value < 1){
                System.out.println("Деньги не внесены");
                System.out.println("Всего транзакций выполнено: " + this.transaction);
            }
            else if (value <= (maxMoney - allMoney)) {
                this.transaction ++;
                this.allMoney +=value;
                System.out.println("Внесена сумма: " + value);
                System.out.println("Всего транзакций выполнено: " + this.transaction);
            }
            else {
                this.transaction ++;
                int temp = 0;
                temp = ((allMoney + value) -maxMoney);
                this.allMoney +=value - temp;
                System.out.println("внеcена сумма: " + (value - temp));
                System.out.println("заберите лишние деньги: " + temp);
                System.out.println("Всего транзакций выполнено: " + this.transaction);
            }



        }

}
